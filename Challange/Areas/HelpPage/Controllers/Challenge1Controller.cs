﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Challange.Areas.HelpPage.Controllers
{
    public class Challenge1Controller : ApiController
    {
        public HttpResponseMessage Problem1(int[] inputArray, int iteration)
        {
            var lengthOfInput = inputArray.Length;
            int[] newArray = new int[lengthOfInput];
            return Request.CreateResponse(HttpStatusCode.OK, new { oldArray = inputArray, newArray = newArray });
        }
    }
}
